import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { User } from '../User';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  user: User;
  disp_msg!: String;

  constructor(
    private AuthenticationService: AuthenticationService,
    private router: Router
  ) {
    this.user = new User();
  }

  
  ngOnInit(): void {}

  register() {
    this.AuthenticationService.registerUser(this.user).subscribe({
      next: (data) => {
        alert(`${this.user.userName} You Have been Succesfully Register Now You can Login`);
        this.router.navigate(['/login']);
      },
      error: (e) => {
        console.log(e);
        this.disp_msg =
          'Failed to create account ! Reason: User Already Exists with this Username';
      },
    });
  }
  
}

